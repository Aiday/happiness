package BinarySearch;

public class BinarySearch {
    public void sort(Data data) {
        for(int i=0; i<data.size; i++ ){
            for(int j=0; j<data.size-i-1; j++){
                if(data.array[j]>data.array[j+1]){
                    int temp=data.array[j];
                    data.array[j]=data.array[j+1];
                    data.array[j+1]=temp;
                }
            }
        }
        for(int i=0; i<data.size; i++){
    System.out.print(data.array[i] + " ");
        }
        System.out.println(" ");
    }
    public int search(Data data, int min, int max) {

        int middle = min + (max - min) / 2;
        if (data.value < data.array[middle]) {
            return search(data, min, middle - 1);
        } else if (data.value > data.array[middle]) {
            return search(data, middle + 1, max);
        } else {
            return middle;
        }
    }
    public  int search(Data data) {
        return search(data, 0, data.array.length-1);
    }
}
