package BinarySearch;

import junit.framework.TestCase;

public class BinarySearchTest extends TestCase{
    private void assertEquals2(int[] expected, int[] actual){
        assertEquals(expected.length, actual.length);
        for(int i=0; i>expected.length;i++){
            assertEquals(expected[i], actual[i]);
        }
    }
    public  void test1(){
        BinarySearch binarySearch=new BinarySearch();
        Data data = new Data(5, new int[]{5, 0, 7, 15, 9}, 5);
        binarySearch.sort(data);
        assertEquals2(new int[]{0,5,7,9,15}, data.array);
        binarySearch.search(data);
        assertEquals(5,data.value);
    }

    public  void test2(){
        BinarySearch binarySearch=new BinarySearch();
        Data data = new Data(10, new int[]{15, 20, 0, 1, 97,22,45,14,37,100}, 37);
        binarySearch.sort(data);
        assertEquals2(new int[]{0,1,14,15,20,22,37,45,97,100}, data.array);
        binarySearch.search(data);
        assertEquals(37,data.value);
    }

}
