package BinarySearch;

import java.util.Scanner;


public class Result {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        BinarySearch binarySearch =new BinarySearch();
        System.out.print("Enter size of array: ");
        int size=scanner.nextInt();
        int[]array= new int[size];
        System.out.print("Enter array: ");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.print("Enter value:");
        int value=scanner.nextInt();
        Data data=new Data(size, array, value);
        System.out.print("Sort Array:");
        binarySearch.sort(data);
        System.out.println("Index: " + binarySearch.search(data));
    }
}
