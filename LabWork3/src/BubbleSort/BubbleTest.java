package BubbleSort;

import junit.framework.TestCase;

public class BubbleTest extends TestCase {
    private void assertEquals(int[] expected, int[] actual){
        assertEquals(expected.length, actual.length);
        for(int i=0; i>expected.length;i++){
            assertEquals(expected[i], actual[i]);
        }
    }

    public void test0() {
        BubleSort bubleSort = new BubleSort();
        Data data = new Data(5, new int[]{5, 0, 7, 15, 9});
        bubleSort.bSort(data);
        assertEquals(new int[]{0,5,7,9,15}, data.array);
    }

    public void test1() {
        BubleSort bubleSort = new BubleSort();
        Data data = new Data(10, new int[]{25, 100, 17, 2, 39, 4, 0, 15, 88, 37});
        bubleSort.bSort(data);
        assertEquals(new int[]{0,2,4,15,17,25,37,39,88,100}, data.array);
    }



}
