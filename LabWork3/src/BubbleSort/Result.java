package BubbleSort;

import java.util.Scanner;

public class Result {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        BubleSort bubleSort =new BubleSort();
        System.out.print("Enter size of array: ");
        int size=scanner.nextInt();
        System.out.print("Enter array: ");
        int[]array=new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }
        Data data=new Data(size, array);
        System.out.print("New array: ");
        bubleSort.bSort(data);

    }
}
