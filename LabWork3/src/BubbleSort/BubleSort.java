package BubbleSort;


public class BubleSort {
    public void bSort(Data data) {
        for(int i=0; i<data.size; i++ ){
            for(int j=0; j<data.size-i-1; j++){
                if(data.array[j]>data.array[j+1]){
                    int temp=data.array[j];
                    data.array[j]=data.array[j+1];
                    data.array[j+1]=temp;
                }
            }
        }
        for (int i = 0; i < data.size; i++) {
            System.out.print(data.array[i] + "  ");
        }
    }

}
