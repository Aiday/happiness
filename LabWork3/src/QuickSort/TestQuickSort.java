package QuickSort;

import junit.framework.TestCase;

public class TestQuickSort extends TestCase {
    private void assertEquals(int[] expected, int[] actual){
        assertEquals(expected.length, actual.length);
        for(int i=0; i>expected.length;i++){
            assertEquals(expected[i], actual[i]);
        }
    }
    public void test0() {
        QuickSort quickSort = new QuickSort();
        Data data = new Data(5, new int[]{5, 0, 7, 15, 9});
        quickSort.qSort(data,0,5);
        assertEquals(new int[]{0,5,7,9,15}, data.array);
    }

    public void test1() {
        QuickSort quickSort = new QuickSort();
        Data data = new Data(10, new int[]{95, 10, 107, 21, 74, 4, 0, 1, 28, 32});
        quickSort.qSort(data,0,10);
        assertEquals(new int[]{0,1,4,10,21,28,32,74,95,107}, data.array);
    }

    public void test2() {
        QuickSort quickSort = new QuickSort();
        Data data = new Data(10, new int[]{25, 100, 17, 2, 39, 4, 0, 15, 88, 37});
        quickSort.qSort(data,0,10);
        assertEquals(new int[]{0,2,4,15,17,25,37,39,88,100}, data.array);
    }


}
