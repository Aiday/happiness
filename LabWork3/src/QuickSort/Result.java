package QuickSort;

import java.util.Scanner;

public class Result {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        QuickSort quickSort=new QuickSort();
        System.out.print("Enter size of array: ");
        int size=in.nextInt();
        System.out.print("Enter array: ");
        int[]array=new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = in.nextInt();
        }
        Data data=new Data(size,array);
        quickSort.qSort(data,0,array.length);
        System.out.print ("New array: ");
        for(int i = 0; i < array.length; i++)
        {
            System.out.print(array[i] + " ");
        }

    }
}
