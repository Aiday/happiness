package QuickSort;

public class QuickSort {

    public  void qSort(Data data, int start, int end) {
        if (start < end - 1) {
            int med = start + (end - start) / 2;
            int i = start, j = end - 1;
            while (i < j) {
                for (i = start; i < med; i++) {
                    if (data.array[i] > data.array[med]) {
                        break;
                    }
                }
                for (j = end - 1; j > med; j--) {
                    if (data.array[j] < data.array[med]) {
                        break;
                    }

                }

                if (i != j) {
                    int m = data.array[i];
                    data.array[i] = data.array[j];
                    data.array[j] = m;
                }

                if (j == med) {
                    med = i;
                } else if (i == med) {
                    med = j;
                }
            }
            qSort(data, start, med);
            qSort(data, med, end);
        }
    }

}
