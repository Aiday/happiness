package Fibbonachi;
import junit.framework.TestCase;

/**
 * Created by user on 04.04.2017.
 */
public class FibbonachiTest extends TestCase {
    public void testFibb1(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(1, fibonacci.getFibb(1));
    }
    public void testFibb2(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(1, fibonacci.getFibb(2));
    }
    public void testFibb3(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(2, fibonacci.getFibb(3));
    }
    public void testFibb4(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(5, fibonacci.getFibb(5));
    }
    public void testFibb5(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(55, fibonacci.getFibb(10));
    }
    public void testFibb6(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(610, fibonacci.getFibb(15));
    }
    public void testFibb7(){
        Fibbonachi fibonacci = new Fibbonachi();
        assertEquals(6765, fibonacci.getFibb(20));
    }

}
