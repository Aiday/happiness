package Factorial;

import junit.framework.TestCase;

public class FactorialTest extends TestCase {
    public  void test1(){
        Factorial fact=new Factorial();
        assertEquals(1, fact.getFactorial(1));
    }
    public  void test2(){
        Factorial fact=new Factorial();
        assertEquals(120, fact.getFactorial(5));
    }
    public  void test3(){
        Factorial fact=new Factorial();
        assertEquals( 40320, fact.getFactorial(8));
    }
    public  void test4(){
        Factorial fact=new Factorial();
        assertEquals(3628800, fact.getFactorial(10));
    }
}
