package Factorial;
import java.util.Scanner;

/**
 * Created by user on 02.03.2017.
 */
public class Result {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        Factorial solution=new Factorial();
        System.out.print("Enter N: ");
        int num=in.nextInt();
        int fact=solution.getFactorial(num);
    System.out.println("Factorial of " + num + " is " + fact);

    }
}

