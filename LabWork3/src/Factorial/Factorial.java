package Factorial;

public class Factorial {
    public  int  getFactorial(int num){
        int fact;
        if (num == 1)
            return 1;
        fact = getFactorial(num-1) * num;
        return fact;
    }
}


