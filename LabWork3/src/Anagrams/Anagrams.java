package Anagrams;

/**
 * Created by user on 12.03.2017.
 */
public class Anagrams {
        public String sort(Data data) {
            char[] str = data.str.toCharArray();
            for(int i = str.length-1 ; i > 0 ; i--){
                for(int j = 0 ; j < i ; j++){
                    if( str[j] < str[j+1] ){
                        char temp = str[j];
                        str[j] = str[j+1];
                        str[j+1] = temp;
                    }
                }
            }
            String newStr = String.valueOf(str);
            return newStr;

        }

    }
