package Anagrams;

import java.util.Scanner;

public class Result {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Anagrams ang= new Anagrams();
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        Data data1= new Data(str1);
        Data data2= new Data(str2);
        String s1=ang.sort(data1);
        String s2=ang.sort(data2);

        if(s1.equals(s2))
            System.out.print("These words are anagrams");
        else
            System.out.print("These words are not anagrams");
    }
}

